maintainer        "Jordan Saunders"
maintainer_email  "jordan@jsaundersdev.com"
license           "Apache 2.0"
description       "Ruby Gem configuration"
long_description  IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version           "0.0.1"

